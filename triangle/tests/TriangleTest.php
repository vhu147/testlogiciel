<?php
use PHPUnit\Framework\TestCase;
require_once 'src/Triangle.php';
class TriangleTest extends TestCase{
    private $triangle;
    public function setUp(){
        $this->triangle = new Triangle();
    }

    /**
     * @dataProvider provider
     */
    public function testInvalideSides($a,$b,$c){
        $this->expectException(Exception::class);
        $this->triangle->setSides($a,$b,$c);
        $this->triangle->analyseType();
    }
    /**
     * Undocumented function
     *
     * @return void
     */
    public function provider(){
        return array(
            array(0,0,0),
            array(0,1,1),
            array(1,2,4),
            array(1,1,2)
        );
    }

    /**
     * Undocumented function
     *
     * @return void
     */
    public function testEquilateral(){
        $this->triangle->setSides(5,5,5);
        $this->assertEquals('equilateral',$this->triangle->analyseType());
    }

    /**
     * Undocumented function
     *
     * @return void
     */
    public function testIsoselecs(){
        $this->triangle->setSides(5,5,9);
        $this->assertEquals('isoselecs',$this->triangle->analyseType());
    }

    /**
     * Undocumented function
     *
     * @return void
     */
    public function testScalene(){
        $this->triangle->setSides(5,4,2);
        $this->assertEquals('scalene',$this->triangle->analyseType());
    }

    public function tearDown(){
        unset($this->triangle);
    }
}
?>