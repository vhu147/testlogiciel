<?php
class Triangle{
    private $a;
    private $b;
    private $c;

    public function setSides($a,$b,$c){
        $this->a = $a;
        $this->b = $b;
        $this->c = $c;
    }

    public function getA(){
        return $this->a;
    }
    public function getB(){
        return $this->a;
    }
    public function getC(){
        return $this->a;
    }
    public function analyseType() : string {
        if($this->a <= 0 or $this->b <= 0 or $this->c <= 0){
            throw new Exception('Negative/Null value of sides triangle.');
        }
        else{
            $sides = [$this->a,$this->b,$this->c];
            sort($sides);
            if ($sides[2] >= $sides[0] + $sides[1]) {
                throw new Exception('This violates the triangle inequality.');
            }
            elseif($this->a == $this->b and $this->a == $this->c){
                return 'equilateral';
            }
            elseif($this->a == $this->b or $this->b == $this->c or $this->a == $this->c){
                return 'isoselecs';
            }
            else{
                return 'scalene';
            }
        }
    }
}
?>