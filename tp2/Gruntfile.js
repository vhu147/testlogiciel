module.exports = function(grunt) {
  grunt.initConfig({
    pkg: grunt.file.readJSON('package.json'),
    concat: {
      options: {
        stripBanners: true,
        banner: '/*! <%= pkg.name %> - v<%= pkg.version %> - ' + '<%= grunt.template.today("yyyy-mm-dd") %> */',
      },
      dist: {
        src: ['js/*.js','tests/*.js'],
        dest: 'dist/<%=pkg.name%>-<%= pkg.version %>.js',
      },
    },
    uglify: {
      options: {
        banner: '/*! <%= pkg.name %> - v<%= pkg.version %> - ' + '<%= grunt.template.today("yyyy-mm-dd") %> */'
      },
      my_target: {
        options: {
          beautify: {
            width: 80
          }
        },
        files: {
          'dist/<%=pkg.name%>-<%= pkg.version %>.min.js': ['js/*.js','tests/*.js']
        }
      }
    },
  })
  grunt.loadNpmTasks('grunt-contrib-concat');
  grunt.loadNpmTasks('grunt-contrib-uglify');
};
