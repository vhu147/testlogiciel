/*! tp2 - v0.0.1 - 2019-12-10 */function bonjour(mess) {
    return str.concat("Bonjour ",mess);
}
function isOdd(x) {
    return x % 2;
}

module.exports = isOdd;
var isOdd = require('../js/isOdd');
QUnit.test( "hello test", function( assert ) {
    assert.ok( 1 == "1", "Passed!" );
});

QUnit.test("Test pair number", function(assert){
    assert.expect(6);
    assert.equal(isOdd(6),0);
    assert.equal(isOdd(2),0);
    assert.equal(isOdd(4),0);
    assert.equal(isOdd(8),0);
    assert.equal(isOdd(10),0);
    assert.equal(isOdd(1024),0);
});

