<?php
/**
 * Gestion de poneys
 */
class Poneys
{
    private $count = 8;

    /**
     * Retourne le nombre de poneys
     *
     * @return int
     */
    public function getCount(): int
    {
        return $this->count;
    }

    /**
     * Retourne le nombre de poneys
     *
     * @return void
     */
    public function setCount($count): void
    {
        $this->count = $count;
    }

    /**
     * Retire un poney du champ
     *
     * @param int $number Nombre de poneys à retirer
     *
     * @return void
     */
    public function removePoneyFromField(int $number): void
    {
        if($number <= $this->count){
            $this->count -= $number;
        }
        else{
            throw new Exception('Number poneys removed exceed.');
        }
    }

    /**
     * Ajouter un certains poneys
     *
     * @param int $number Nombre de poneys à ajouter
     *
     * @return void
     */
    public function addPoneys(int $number): void {
        $this->count += $number;
    }

    /**
     * Retourne les noms des poneys
     *
     * @return array
     */
    public function getNames(): array
    {

    }

    /**
     * Verify if herf of poneys is full
     *
     * @return bool
     */
    public function isFull(): bool {
        if($this->getCount() >= 15){
            return TRUE;
        }
        else{
            return FALSE;
        }
    }
}
?>
