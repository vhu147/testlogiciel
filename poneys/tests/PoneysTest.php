<?php
use PHPUnit\Framework\TestCase;

require_once 'src/Poneys.php';

/**
 * Classe de test de gestion de poneys
 */
class PoneysTest extends TestCase
{
    private $poneys;
    private $tailleChamps;
    /**
     * Undocumented function
     *
     * @return void
     */
    public function testRemovePoneyFromField()
    {
        // Setup

        // Action
        $this->poneys->removePoneyFromField(3);

        // Assert
        $this->assertEquals(5, $this->poneys->getCount());
    }

    /**
     * Undocumented function
     *
     * @return void
     */
    public function testAddPoney()
    {
        $tempCount = $this->poneys->getCount();
        $this->poneys->addPoneys(3);        

        $this->assertEquals($tempCount + 3, $this->poneys->getCount());
    }

    /**
     * Undocumented function
     *
     * @return void
     */
    public function testRemovePoneysNegative(){
        $this->expectException(Exception::class);
        $this->poneys->removePoneyFromField(9);
    }

    /**
     * @dataProvider provider
     */
    public function testDataProvider($remove, $count){
        $this->poneys->RemovePoneyFromField($remove);
        $this->assertEquals($count,$this->poneys->getCount());
    }

    /**
     * Undocumented function
     *
     * @return void
     */
    public function provider(){
        return array(
            array(0,8),
            array(1,7),
            array(5,3),
            array(8,0)
        );
    }

    public function testMockGetNames(){
        $names=['a','b','c'];
        $mock = $this->getMockBuilder('Poneys')->getMock();
        $mock->expects($this->once())->method('getNames')->willReturn($names);
        $this->assertEquals($names, $mock->getNames());
    }

    public function testIsFullTrue(){
        $this->poneys->addPoneys(7);
        $this->assertTrue($this->poneys->isFull());
    }

    protected function setUp(){
        $this->poneys = new Poneys();
        $this->poneys->setCount(8);
        $this->tailleChamps = tailleChamp;
    }

    protected function tearDown(){
        unset($this->poneys);
    }
}
?>
