<?php
use PHPUnit\Framework\TestCase;
require_once 'src/Poneys.php';
class MyTest extends TestCase{
    private $poneys;
    public function setUp(){
        $this->poneys = new Poneys();
        $this->poneys->setCount(10);
    }

    public function testMyTest(){
        $this->assertEquals(10,$this->poneys->getCount());
    }

    public function tearDown(){
        unset($this->poneys);
    }
}
?>